#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-11-10 13:54:27
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


"""为python代码添加计时"""


from uuid import uuid4

import sublime
import sublime_plugin


class ToggleTimepointCommand(sublime_plugin.TextCommand):

    @property
    def start_timepoint(self):
        return "import time; t1 = time.time()\n"

    @property
    def end_timepoint(self):
        return "t2 = time.time() - t1; print(t2)\n"

    def _add_time_line(self, line):

        indent = self._get_indent(line)
        self.view.insert(
            self._edit,
            line.end(),
            "\n" + " "*indent + self.end_timepoint
        )
        self.view.insert(
            self._edit,
            line.begin(),
            " "*indent + self.start_timepoint
        )
        self.view.add_regions(
            str(uuid4()),
            [self.view.line(line.begin())],
            "invalid",
            "circle",
            sublime.PERSISTENT
        )

    def _add_time_region(self, start, end):

        start_indent = self._get_indent(start)
        end_indent = self._get_indent(end)

        self.view.insert(
            self._edit,
            end.end(),
            "\n" + " "*end_indent + self.end_timepoint
        )
        self.view.insert(
            self._edit,
            start.begin(),
            " "*start_indent + self.start_timepoint
        )
        self.view.add_regions(
            str(uuid4()),
            [self.view.line(start.begin())],
            "invalid",
            "circle",
            sublime.PERSISTENT
        )

    def _get_indent(self, line):

        return (
            len(self.view.substr(line))
            - len(self.view.substr(line).lstrip())
        )

    def _is_python(self):

        return self.view.match_selector(0, "source.python")

    def _save(self):

        self.view.run_command("save")

    def run(self, edit):

        self._edit = edit

        if not self._is_python():
            return

        if self.view.sel()[0].empty():
            self._add_time_line(self.view.line(self.view.sel()[0]))
        else:
            start_line = self.view.line(self.view.sel()[0].begin())
            end_line = self.view.line(self.view.sel()[0].end())
            self._add_time_region(start_line, end_line)

        self._save()
